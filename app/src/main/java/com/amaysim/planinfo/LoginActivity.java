package com.amaysim.planinfo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.amaysim.planinfo.helpers.JSONHelper;
import com.amaysim.planinfo.helpers.LoginHelper;
import com.amaysim.planinfo.settings.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class LoginActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.msnText).requestFocus();

    }

    public void onClickLogin(View v) throws IOException, JSONException {
        String msnText = ((EditText) findViewById(R.id.msnText))
                .getText()
                .toString();

        if(!LoginHelper.checkDigits(msnText)){
            Toast.makeText(this, R.string.txtMsnWrong, Toast.LENGTH_LONG).show();
        }else{
            JSONObject msnJSONData = new JSONObject(JSONHelper.loadJson(
                    getAssets().open(Constants.COLLECTION_FILE_NAME)));

            if(LoginHelper.authenticateNumber(msnText, msnJSONData)){
                startActivity(new Intent(LoginActivity.this, SplashActivity.class));
            }else{
                Toast.makeText(this, R.string.txtMsnNotFound, Toast.LENGTH_LONG).show();
            }


        }
    }


}
