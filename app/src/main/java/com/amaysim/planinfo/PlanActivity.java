package com.amaysim.planinfo;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.amaysim.planinfo.fragments.AccountDataFragment;
import com.amaysim.planinfo.fragments.SubscriptionFragment;
import com.amaysim.planinfo.helpers.IncludeIdentifier;
import com.amaysim.planinfo.helpers.JSONHelper;
import com.amaysim.planinfo.helpers.PlanHelpers;
import com.amaysim.planinfo.helpers.SubscriptionInfo;
import com.amaysim.planinfo.settings.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class PlanActivity extends AppCompatActivity {

    AccountDataFragment adFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);

        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void init() throws IOException, JSONException {
        JSONObject msnJSONData = new JSONObject(JSONHelper.loadJson(
                                                getAssets().open(Constants.COLLECTION_FILE_NAME)));

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        adFragment = new AccountDataFragment().newInstance(msnJSONData.getJSONObject("data"));
        fragmentTransaction.add(R.id.ac_data_group, adFragment);

        HashMap<IncludeIdentifier, JSONObject> includesMap
                = PlanHelpers.generateInclMap(msnJSONData.getJSONArray("included"));

        ArrayList<IncludeIdentifier> servicesList
                = PlanHelpers.getServicesIncls(msnJSONData.getJSONArray("included"));


        for(IncludeIdentifier iid : servicesList){

            JSONArray subscriptions = includesMap.get(iid)
                    .getJSONObject("relationships")
                    .getJSONObject("subscriptions")
                    .getJSONArray("data");

            for(int x = 0; x<subscriptions.length(); x++){
                SubscriptionInfo si = PlanHelpers.getInfoFromSubscription(
                        new IncludeIdentifier(subscriptions.getJSONObject(x).getString("type"),
                                subscriptions.getJSONObject(x).getString("id")), includesMap);

                SubscriptionFragment sFragment = new SubscriptionFragment().newInstance(si);
                fragmentTransaction.add(R.id.subs_group, sFragment);


            }
        }


        fragmentTransaction.commit();

    }
}
