package com.amaysim.planinfo.fragments;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amaysim.planinfo.R;
import com.amaysim.planinfo.helpers.SubscriptionInfo;

import org.json.JSONException;

public class SubscriptionFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PRODUCT_NAME = "PRODUCT_NAME";
    private static final String PRODUCT_PRICE = "PRODUCT_PRICE";
    private static final String REMAINING_BALANCE = "REMAINING_BALANCE";
    private static final String EXPIRY_DATE = "EXPIRY_DATE";
    private static final String IS_AUTO_RENEW = "IS_AUTO_RENEW";

    private String mProductName;
    private String mProductPrice;
    private String mRemainingBalance;
    private String mExpiryDate;
    private String mIsAutoRenew;

    private OnFragmentInteractionListener mListener;

    public SubscriptionFragment() {
        // Required empty public constructor
    }

    public static SubscriptionFragment newInstance(SubscriptionInfo si) throws JSONException{
        SubscriptionFragment fragment = new SubscriptionFragment();
        Bundle args = new Bundle();

        args.putString(PRODUCT_NAME, si.getProductName());
        args.putString(PRODUCT_PRICE, si.getPrice().toString());
        args.putString(REMAINING_BALANCE, si.getIncludedDataBalance().toString());
        args.putString(EXPIRY_DATE, si.getSubscriptionEndDate());
        args.putString(IS_AUTO_RENEW, si.isAutoRenewal() ? "Auto Renew" : "Not Auto Renew");

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductName = getArguments().getString(PRODUCT_NAME);
            mProductPrice = getArguments().getString(PRODUCT_PRICE);
            mRemainingBalance = getArguments().getString(REMAINING_BALANCE);
            mExpiryDate = getArguments().getString(EXPIRY_DATE);
            mIsAutoRenew = getArguments().getString(IS_AUTO_RENEW);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_subscription, container, false);

        TextView productName = (TextView) view.findViewById(R.id.txtProductName);
        TextView productPrice = (TextView) view.findViewById(R.id.txtProductPrice);
        TextView remainingBalance = (TextView) view.findViewById(R.id.txtRemainingBalance);
        TextView subscriptionExpiry = (TextView) view.findViewById(R.id.txtSubscriptionExpiry);
        TextView isAutoRenewal = (TextView) view.findViewById(R.id.txtIsAutoRenewal);

        productName.setText(mProductName);
        productPrice.setText("$" + mProductPrice);
        remainingBalance.setText(mRemainingBalance + " GB");
        subscriptionExpiry.setText("Will expire at " + mExpiryDate);
        isAutoRenewal.setText(mIsAutoRenew);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
