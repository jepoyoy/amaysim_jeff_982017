package com.amaysim.planinfo.fragments;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amaysim.planinfo.R;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountDataFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ACCOUNT_FNAME = "ACCOUNT_FNAME";
    private static final String ACCOUNT_LNAME = "ACCOUNT_LNAME";
    private static final String ACCOUNT_TYPE = "ACCOUNT_TYPE";
    private static final String UNBILLED_CHARGES = "UNBILLED_CHARGES";
    private static final String NEXT_BILLING_DATE = "NEXT_BILLING_DATE";

    private String mAccountFname;
    private String mAccountLname;
    private String mAccountType;
    private String mUnbilledCharges;
    private String mNextBillingDate;

    private OnFragmentInteractionListener mListener;

    public AccountDataFragment() {
        // Required empty public constructor
    }

    public static AccountDataFragment newInstance(JSONObject accountsJSON) throws JSONException{
        AccountDataFragment fragment = new AccountDataFragment();
        Bundle args = new Bundle();

        args.putString(ACCOUNT_FNAME, accountsJSON.getJSONObject("attributes").getString("first-name"));
        args.putString(ACCOUNT_LNAME, accountsJSON.getJSONObject("attributes").getString("last-name"));
        args.putString(ACCOUNT_TYPE, accountsJSON.getJSONObject("attributes").getString("payment-type"));
        args.putString(UNBILLED_CHARGES, accountsJSON.getJSONObject("attributes").getString("unbilled-charges"));
        args.putString(NEXT_BILLING_DATE, accountsJSON.getJSONObject("attributes").getString("next-billing-date"));

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAccountFname = getArguments().getString(ACCOUNT_FNAME);
            mAccountLname = getArguments().getString(ACCOUNT_LNAME);
            mAccountType = getArguments().getString(ACCOUNT_TYPE);
            mUnbilledCharges = getArguments().getString(UNBILLED_CHARGES);
            mNextBillingDate = getArguments().getString(NEXT_BILLING_DATE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_account_data, container, false);

        TextView accountName = (TextView) view.findViewById(R.id.txtAccountName);
        TextView accountType = (TextView) view.findViewById(R.id.txtAccountType);
        TextView unbilled = (TextView) view.findViewById(R.id.txtUnbilledCharges);
        TextView nextBillingDate = (TextView) view.findViewById(R.id.txtNextBillingDate);

        accountName.setText(mAccountFname + " " + mAccountLname);
        accountType.setText(mAccountType);
        unbilled.setText(mUnbilledCharges);
        nextBillingDate.setText(mNextBillingDate);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
