package com.amaysim.planinfo.helpers;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by user on 8/9/2017.
 */

public class JSONHelper {

    public static String loadJson(InputStream in){
        byte[] buffer;
        try {
            int size = in.available();
            buffer = new byte[size];
            in.read(buffer);
            in.close();
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
