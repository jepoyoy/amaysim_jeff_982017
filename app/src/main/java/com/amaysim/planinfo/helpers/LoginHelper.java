package com.amaysim.planinfo.helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 7/9/2017.
 */

public class LoginHelper {

    static public boolean checkDigits(String text){

        Pattern checkRegex = Pattern.compile("^[0-9][0-9]{9}$");
        Matcher regexMatcher = checkRegex.matcher(text);

        while(regexMatcher.find()){
            if(regexMatcher.group().length()!=0){
                return true;
            }
        }
        return false;
    }

    public static boolean authenticateNumber(String msn, JSONObject msnJSONData)
            throws JSONException {
        if(msn.equals(msnJSONData.getJSONObject("data")
                .getJSONObject("attributes")
                .getString("contact-number"))){
            return true;
        }

        return false;
    }
}
