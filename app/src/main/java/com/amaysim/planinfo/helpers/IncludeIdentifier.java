package com.amaysim.planinfo.helpers;

/**
 * Created by user on 8/9/2017.
 */

public class IncludeIdentifier {

    private String type;
    private String id;

    public IncludeIdentifier(String type, String id) {
        this.type = type;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return type.hashCode() + id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        return this.getId().equals(((IncludeIdentifier)obj).getId()) &&
                this.getType().equals(((IncludeIdentifier)obj).getType());
    }
}
