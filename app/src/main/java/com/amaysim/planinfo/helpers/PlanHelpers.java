package com.amaysim.planinfo.helpers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 7/9/2017.
 */

public class PlanHelpers {

    public static HashMap<IncludeIdentifier, JSONObject> generateInclMap(JSONArray accountIncludes){
        HashMap<IncludeIdentifier, JSONObject> includesMap = new HashMap<>();

        try {
            for (int i = 0; i < accountIncludes.length(); i++) {
                JSONObject incl = accountIncludes.getJSONObject(i);
                IncludeIdentifier iid = new IncludeIdentifier(incl.getString("type"), incl.getString("id"));
                includesMap.put(iid, incl);
            }
        }catch (JSONException e) {

        }
        return includesMap;
    }

    public static ArrayList<IncludeIdentifier> getServicesIncls(JSONArray accountIncludes){
        ArrayList<IncludeIdentifier> servicesList = new ArrayList<>();

        try {
            for (int i = 0; i < accountIncludes.length(); i++) {
                JSONObject incl = accountIncludes.getJSONObject(i);
                if(incl.getString("type").equalsIgnoreCase("services")) {
                    IncludeIdentifier iid = new IncludeIdentifier(incl.getString("type"), incl.getString("id"));
                    servicesList.add(iid);
                }
            }
        }catch (JSONException e) {

        }
        return servicesList;
    }

    public static SubscriptionInfo getInfoFromSubscription(IncludeIdentifier ii,
                                                      HashMap<IncludeIdentifier, JSONObject> includesMap)
    {

        SubscriptionInfo si = new SubscriptionInfo();

        try {

            JSONObject subscription = (JSONObject) includesMap.get(ii);

            si.setIncludedDataBalance(subscription.getJSONObject("attributes").getInt("included-data-balance") / 1000d);
            si.setSubscriptionEndDate(subscription.getJSONObject("attributes").getString("expiry-date"));
            si.setAutoRenewal(subscription.getJSONObject("attributes").getBoolean("auto-renewal"));

            JSONObject subscriptionRelationshipData = (JSONObject) subscription
                    .getJSONObject("relationships")
                    .getJSONObject("product")
                    .getJSONObject("data");

            JSONObject product = includesMap.get(
                    new IncludeIdentifier(subscriptionRelationshipData.getString("type"),
                            subscriptionRelationshipData.getString("id")));

            si.setProductName(product.getJSONObject("attributes").getString("name"));
            si.setPrice(product.getJSONObject("attributes").getInt("price")*0.01d);

        }catch (JSONException e){

        }
        return si;
    }
}
