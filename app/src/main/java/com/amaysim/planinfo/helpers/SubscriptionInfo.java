package com.amaysim.planinfo.helpers;

/**
 * Created by user on 8/9/2017.
 */

public class SubscriptionInfo {

    Double includedDataBalance;
    boolean autoRenewal;
    String productName;
    Double price;
    String subscriptionEndDate;

    public Double getIncludedDataBalance() {
        return includedDataBalance;
    }

    public void setIncludedDataBalance(Double includedDataBalance) {
        this.includedDataBalance = includedDataBalance;
    }

    public boolean isAutoRenewal() {
        return autoRenewal;
    }

    public void setAutoRenewal(boolean autoRenewal) {
        this.autoRenewal = autoRenewal;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSubscriptionEndDate() {
        return subscriptionEndDate;
    }

    public void setSubscriptionEndDate(String subscriptionEndDate) {
        this.subscriptionEndDate = subscriptionEndDate;
    }
}
