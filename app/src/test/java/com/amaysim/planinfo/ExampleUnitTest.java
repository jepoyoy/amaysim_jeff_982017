package com.amaysim.planinfo;

import com.amaysim.planinfo.helpers.LoginHelper;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void testRegex(){
        assertFalse(LoginHelper.checkDigits("A"));
        assertFalse(LoginHelper.checkDigits("ABCD010129"));
        assertFalse(LoginHelper.checkDigits("046887"));
        assertFalse(LoginHelper.checkDigits(""));
        assertTrue(LoginHelper.checkDigits("0468874507"));
    }
}