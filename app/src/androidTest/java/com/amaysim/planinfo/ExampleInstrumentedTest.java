package com.amaysim.planinfo;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.amaysim.planinfo.helpers.IncludeIdentifier;
import com.amaysim.planinfo.helpers.JSONHelper;
import com.amaysim.planinfo.helpers.LoginHelper;
import com.amaysim.planinfo.helpers.PlanHelpers;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.amaysim.planinfo", appContext.getPackageName());
    }

    @Test
    public void testFetchingJson() throws Exception{
        Context appContext = InstrumentationRegistry.getTargetContext();
        String str = JSONHelper.loadJson(appContext.getAssets().open("collection.json"));
        JSONObject obj = new JSONObject(str);
        assertNotNull(obj);
        assertNotNull(obj.getJSONObject("data"));
    }

    @Test
    public void testFetchedInclMap() throws Exception{
        Context appContext = InstrumentationRegistry.getTargetContext();
        String str = JSONHelper.loadJson(appContext.getAssets().open("collection.json"));
        JSONObject obj = new JSONObject(str);
        HashMap<IncludeIdentifier, JSONObject> includesMap
                = PlanHelpers.generateInclMap(obj.getJSONArray("included"));
        assertEquals(includesMap.size(),3);
    }

    @Test
    public void testFetchServiceInclList() throws Exception{
        Context appContext = InstrumentationRegistry.getTargetContext();
        String str = JSONHelper.loadJson(appContext.getAssets().open("collection.json"));
        JSONObject obj = new JSONObject(str);
        ArrayList<IncludeIdentifier> list
                = PlanHelpers.getServicesIncls(obj.getJSONArray("included"));
        assertEquals(list.size(),1);
        assertEquals(list.get(0).getType(),"services");
        assertEquals(list.get(0).getId(),"0468874507");
    }

    @Test
    public void testAuthContactNumber() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        String str = JSONHelper.loadJson(appContext.getAssets().open("collection.json"));
        JSONObject obj = new JSONObject(str);
        assertTrue(LoginHelper.authenticateNumber("0468874507",obj));
    }
}
