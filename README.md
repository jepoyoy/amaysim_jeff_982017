# README #

Hi Amaysim, Patricia!

Thanks for the opportunity! I've got some clarifications about the submision:

1. The MSN number seems to be missing from the main data node in the Collection.json. After researching on how mobile numbers 
look in australia, I've assumed that the contact-number, to be able to login, must the the same as the MSN number 0468874507.
This is also evident in my opinion in the REST api endpoints that can be seen all throughtout Collection.json. So, I've made the change in the
contact-number to be able to resume development.

2. The product ID seems to be odd in the relationship part of the Subscriptions object. the ID doesn't correlate with the only product present
in the JSOn file. After reading the JSONApi spec, it seems that the ID and type are essential to the relationship attribute, 
and it corresponds to either a REST endpoint, or an existing object in the JSON output. So for the Product, I've changed it to 4000
to be able to continue with the development.

3. The relationship of services to susbcriptions must be one to many. 

"data": [
            {
              "type": "subscriptions",
              "id": "0468874507-0"
            }
          ]
		  
It is the only relationship wherein the data is not a single object, but an array. So I imagined the app would work one person having possibly,
multiple subscriptions.

